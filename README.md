# Streamlit App with a Hugging Face Model

This project is a sentiment analysis tool built with Streamlit and PyTorch, utilizing the `cardiffnlp/twitter-roberta-base-sentiment` model from Hugging Face's Transformers library. It analyzes text input by users to determine its sentiment (positive/negative/neutral) with confidence score, displaying the results in a user-friendly web interface with visual feedback on sentiment intensity.

[Link for Sentiment Analysis Tool](https://miniproj9hw308-aiteplw9je4lo7v3synd5r.streamlit.app)

## Getting started for Instructions

1. Build a conda environment `conda create -name `.
2. Add all the packages required in `requirements.txt`.
3. Implement code in app.py to achieve sentiment analysis tool built using Streamlit and PyTorch with the `cardiffnlp/twitter-roberta-base-sentiment` model.
- Title and Introduction Setup: displaying a title and a brief description of the tool.
- Model Loading: It loads the tokenizer and model for sentiment analysis
- User Input Interface: The code sets up a sidebar in the Streamlit application where users can input the text they want to analyze. It includes a text area for input and a button labeled "Evaluate Sentiment".
- Sentiment Analysis: When the user inputs text and clicks the "Evaluate Sentiment" button, the app tokenizes the input text using the loaded tokenizer, respecting the maximum length limitation and ensuring the text is appropriately prepared for the model. The tokenized text is then fed into the sentiment analysis model.
- Result Processing
4. Git push on github and deploy it on Streamlit. 

## App Functionality

**1. Functioning Web App**

It powers a web-based sentiment analysis tool that allows users to input text and receive an analysis of the sentiment conveyed, complete with a confidence score and intuitive, color-coded feedback.

**2. Connection to Open Source LLM**

It loads the tokenizer and model for sentiment analysis using the RobertaTokenizer and RobertaForSequenceClassification from Hugging Face's Transformers library. The model specifically used here is cardiffnlp/twitter-roberta-base-sentiment, designed for analyzing sentiments in tweets.

**3. Aesthetics/Creativity of Site & Chatbot Performance**

The app maps the model's output (like LABEL_0, LABEL_1, LABEL_2) to human-readable sentiments (negative, neutral, positive) and displays this result on the main page of the web app. It shows the sentiment label, a confidence score, and a progress bar representing the confidence score.

Depending on the predicted sentiment, the app displays a corresponding message with a color-coded alert (success for positive, error for negative, and warning for neutral), giving immediate visual feedback about the sentiment of the entered text.

## Screenshot for Examples
![](images/1.png)
![](images/2.png)
![](images/3.png)
